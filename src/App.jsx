import React, { Component } from "react";
import moment from "moment";

import "./App.css";
import { checkInstallPWA, installPwa } from "./pwa";
import * as timerStates from "./timerStates";
import TimerConfig from "./components/TimerConfig/TimerConfig";
import TimerDisplay from "./components/TimerDisplay/TimerDisplay.jsx";
import TimerControlls from "./components/TimerControlls/TimerControlls";
import Exit from "./components/Exit/Exit";
import Hud from "./components/Hud/Hud";
import Settings from "./components/Settings/Settings";
import SettingsIcon from "./components/SettingsIcon/SettingsIcon";
import { notify } from "./Configuration/Notification";
import { flash } from "./Configuration/Background";
import { save, load } from "./Configuration/LocalStorage";

const finish = require("./assets/sounds/fin.aac");
const start = require("./assets/sounds/start.wav");

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      deferredPropmt: false,
      timer: null,
      flashing: null,
      timerState: timerStates.NOT_SET,
      currentTime: moment.duration(25, "minutes"),
      baseTime: moment.duration(load("baseTime") || 25, "minutes"),
      currentBreak: moment.duration(5, "minutes"),
      baseBreak: moment.duration(load("baseBreak") || 5, "minutes"),
      beforeLongBreak: moment.duration(load("baseBreak") || 5, "minutes"),
      volume: load("volume") || false,
      soundStart: load("volume") ? new Audio(start) : null,
      soundFinish: load("volume") ? new Audio(finish) : null,
      sprint: load("sprint") || 0,
      sprintCount: 0,
      notification: load("notification") || false,
      exit: true,
    };
    
    window.addEventListener('resize', function(e){
      let fixedWidth = 500;
      let fixedHeight = 560;
  
      window.resizeTo(fixedWidth, fixedHeight);
  });

    checkInstallPWA((param) => this.setState({ deferredPropmt: param }));
  }

  play = () => {
    const { baseTime, baseBreak, sprintCount } = this.state;
    this.setState({
      timerState: timerStates.RUNNING,
      currentTime: moment.duration(baseTime.asSeconds(), "seconds"),
      currentBreak: moment.duration(baseBreak.asSeconds(), "seconds"),
      timer: setInterval(this.reduceTimer, 1000),
      sprintCount: sprintCount + 1,
    });
  };

  resume = () => {
    const { timerState } = this.state;
    if (timerState && timerState !== 2) {
      this.setState({
        timerState: timerStates.RESUMED,
        // timer: null,
      });
      clearInterval(this.state.timer);
    } else if (timerState === timerStates.RESUMED) {
      this.setState({
        timerState: timerStates.RUNNING,
        timer: setInterval(this.reduceTimer, 1000),
      });
    } else {
      this.play();
    }
  };

  handleStop = () => {
    const { baseTime, baseBreak, beforeLongBreak, timerState, timer } = this.state;

    /** CLEAR AND RESET STATES AND INTERVAL */
    this.setState(
      {
        timerState: timerStates.NOT_SET,
        currentTime: moment.duration(baseTime.asSeconds(), "seconds"),
        currentBreak: moment.duration(baseBreak.asSeconds(), "seconds"),
        baseBreak: beforeLongBreak,
        timer: null,
        sprintCount: 0,
      },
      () => {
        document.querySelector("body").style.backgroundColor = "#282c34";
        timerState && clearInterval(timer)
      }
    );
  };

  runSprint = () => {
    const { timer, baseTime, baseBreak, timerState } = this.state;

    /** RESET TIMERS */
    this.setState(
      {
        currentTime: moment.duration(baseTime.asSeconds(), "seconds"),
        currentBreak: moment.duration(baseBreak.asSeconds(), "seconds"),
        timer: null,
      },
      () => timerState && clearInterval(timer)
    );
    /** RUN THE TIMERS */
    this.play();
  };

  reduceTimer = () => {
    const { timerState, currentTime, volume, soundFinish, notification } = this.state;

    // Check if work time or break time 
    if (timerState === timerStates.RUNNING) {
      const newTime = moment.duration(currentTime.asSeconds(), "seconds");

      /** SUBTRACT 1 SECOND */
      currentTime.asSeconds() !== 0 && newTime.subtract(1, "second");

      /** UPDATE THE CURRENTTIME'S STATE */
      this.setState({
        currentTime: newTime,
      });

      /** PLAY SOUND */
      if (currentTime.asSeconds() === 1) {
        volume && soundFinish.play();
        notification && notify("Time's up, take a break!");
      }

      /** SET COMPLETED STATE */
      if (newTime.asSeconds() === 0) {
        this.flashBgc();
        this.setState({
          timerState: timerStates.COMPLETED,
        });
      }
    } else if (timerState === timerStates.COMPLETED) {
      const {
        currentTime,
        currentBreak,
        baseBreak,
        beforeLongBreak,
        volume,
        soundStart,
        sprint,
        sprintCount,
        notification,
      } = this.state;
      const newTime = moment.duration(currentBreak.asSeconds(), "seconds");

      /** SUBTRACT 1 SECOND */
      currentTime.asSeconds() > 0 && newTime.subtract(1, "second");

      /** UPDATE CURRENT TIME AND BREAK STATE */
      newTime.asSeconds() >= 0 &&
        this.setState({
          currentBreak: newTime,
          currentTime: newTime,
        });

      /** PLAY SOUND */
      if (newTime.asSeconds() === 1) {
        volume && soundStart.play();
        notification && notify("Finish break, get back to business!");
      }

      /** RE RUN SPRINT */
      if (currentBreak.asSeconds() === 0) {
        if(sprintCount + 1 === sprint) {
          const longBreak = moment.duration(baseBreak.asSeconds(), "seconds");
          longBreak.add(5, "minute");
          this.setState({baseBreak: longBreak});
        }
        if (sprint > sprintCount) {
          this.flashBgc();
          this.runSprint();
        } else {
          this.flashBgc();
          this.setState({sprintCount: 0, baseBreak: beforeLongBreak });
          this.runSprint();
        }
      }
    }
  };

  flashBgc = () => {
    this.setState(
      {
        flashing: setInterval(() => flash(this.state.timerState), 150),
      },
      () =>
        {
          console.log("Set timeout is called!");
          setTimeout(() => {
            console.log("Set timeout is fired!");
          clearInterval(this.state.flashing);
          document.querySelector("body").style.backgroundColor = "#282c34";
        }, 1000)}
    );
  };

  volume = () => {
    const { volume } = this.state;
    this.setState({
      volume: !volume,
      soundFinish: !volume ? new Audio(finish) : null,
      soundStart: !volume ? new Audio(start) : null,
    });
    save("volume", !volume);
  };

  setSprint = async () => {
    const {sprint} = this.state;
    if(sprint < 5) {
      await this.setState({ sprint: sprint + 1 });
    } else {
      await this.setState({ sprint: 0 });
    }

    save("sprint", this.state.sprint);
  };

  render() {
    const {
      deferredPropmt,
      volume,
      sprint,
      notification,
      currentTime,
      timerState,
      baseTime,
      baseBreak,
      exit,
    } = this.state;
    return (
      <main className="App">
        {deferredPropmt && (
          <button className="install" onClick={() => installPwa(deferredPropmt)}>
            install
          </button>
        )}
        <article>
          <SettingsIcon handleOpen={() => this.setState({ exit: false })} />
          {!exit && (
            <Exit handleExit={() => this.setState({ exit: true })}>
              <Hud handleExit={() => this.setState({ exit: true })}>
                <Settings
                  volume={volume}
                  sprint={sprint}
                  notification={notification}
                  setVolume={this.volume}
                  setSprint={this.setSprint}
                  setNotification={(param) => this.setState({ notification: param })}
                />
              </Hud>
            </Exit>
          )}
          <TimerDisplay currentTime={currentTime} />
          {(!timerState || timerState === timerStates.RESUMED) && (
            <TimerConfig
              baseTime={baseTime}
              baseBreak={baseBreak}
              setBaseTime={(param) => this.setState({
                baseTime: param,
              })}
              setBreakTime={(param) =>this.setState({
                baseBreak: param,
                beforeLongBreak: param,
              })}
            />
          )}
          <TimerControlls
            timerState={timerState}
            handleStop={this.handleStop}
            handleResume={this.resume}
          />
        </article>
      </main>
    );
  }
}

export default App;
