export const checkInstallPWA = (func) => {
  window.addEventListener("beforeinstallprompt", (e) => {
    e.preventDefault();
    func(e);

    console.log("PWA: Installable");
  });

  window.addEventListener("appinstalled", (evt) => {
    func(false);

    console.log("PWA INSTALL: Success");
  });
};


export const installPwa = (deferredPrompt) => {
  deferredPrompt.prompt();

  deferredPrompt.userChoice
  .then((choiceResult) => {
    if (choiceResult.outcome === 'accepted') {
      console.log('User accepted the A2HS prompt');
    } else {
      console.log('User dismissed the A2HS prompt');
    }
  });
}