import { RUNNING } from "../timerStates";

export const flash = (state) => {
  const bgc = document.querySelector("body");
  const color = bgc.style.backgroundColor;

  color === "rgba(40, 44, 52, 0.133)"
    ? (bgc.style.backgroundColor =
      state === RUNNING ? "#e9402e" : "#78c13d") // red or green
    : (bgc.style.backgroundColor = "rgba(40, 44, 52, 0.133)"); // white
  };