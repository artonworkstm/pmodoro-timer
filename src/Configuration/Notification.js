const icon = require("../assets/images/tomato.png");

export const notify = (message) => {
	if (Notification.permission === "granted") {
		navigator.serviceWorker.ready.then(function (registration) {
			console.log("serviceworker: ready");
			registration.showNotification(message, {
				icon: icon,
			});
		});
	}
}