export const save = (name, value) => {
	localStorage.setItem(name, JSON.stringify(value));
}

export const load = (name) => {
	return JSON.parse(localStorage.getItem(name));
}